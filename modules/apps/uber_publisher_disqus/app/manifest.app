name = Uber Publisher Disqus
description = "Uber Publisher Disqus comments add the integration with Disqus service"
machine_name = uber_publisher_disqus
version = 7.x-1.0
author = Vardot
author_url = http://www.vardot.com/
logo = logo.png
screenshots[] = screenshot.jpg
downloadable = uber_publisher_disqus 1.0

dependencies[uber_publisher_core] = uber_publisher_core 1.0

downloadables[uber_publisher_comments 7.x-1.0] = http://app_server.local/sites/default/files/uber_publisher_disqus-7.x-1.0.tar.gz
