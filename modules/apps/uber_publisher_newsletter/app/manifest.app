name = Uber publisher newsletter
description = "Uber publisher newsletter"
machine_name = uber_publisher_newsletter
version = 7.x-1.0
author = Vardot
author_url = http://www.vardot.com/
logo = logo.png
screenshots[] = screenshot.jpg
downloadable = uber_publisher_newsletter 1.0

dependencies[uber_publisher_core] = uber_publisher_core 1.0

downloadables[uber_publisher_comments 7.x-1.0] = http://app_server.local/sites/default/files/uber_publisher_newsletter-7.x-1.0.tar.gz
