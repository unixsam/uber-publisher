<div class="<?php print $classes; ?>">
  <div class="screenshot"><?php print $bundle['screenshot']; ?></div>
  <h3><?php print $title; ?></h3>
  <p><?php $bundle['description']; ?></p>
</div>